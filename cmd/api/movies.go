package main

import (
	"fmt"
	"net/http"
	"time"

	"movies.henrique.ie/internal/data"
)

func (app *application) createMovieHandler(w http.ResponseWriter, r *http.Request) {

	//expect data in the req body and target for decoding
	var input struct {
		Title   string       `json:"title"`
		Year    int32        `json:"year"`
		Runtime data.Runtime `json:"runtime"`
		Genres  []string     `json:"genres"`
	}

	err := app.readJSON(w, r, &input)
	if err != nil {
		app.badRequestResponse(w, r, err)
		return
	}

	fmt.Fprintf(w, "%+v\n", input)
	/*
		//struct MUST be passed as a pointer and its fields exported. KV pairs are the reference for naming the struct fields.
		//Since we are decoding the JSON in the block above, we can disable this one -- we cannot read the body twice without using NOPCloser.
		err = json.NewDecoder(r.Body).Decode(&input)
		if err != nil {
			app.errorResponse(w, r, http.StatusBadRequest, err.Error())
			return
		}
		//dump the populated struct into the ResponseWriter
		fmt.Fprintf(w, "%+v\n", input)
	*/
}

func (app *application) showMovieHandler(w http.ResponseWriter, r *http.Request) {

	id, err := app.readIDParam(r)
	if err != nil {
		app.notFoundResponse(w, r)
		return
	}

	//dummy data. without using tags, JSON jeys match the struct field names
	movie := data.Movie{
		ID:        id,
		CreatedAt: time.Now(),
		Title:     "Casablanca",
		Runtime:   102,
		Genres:    []string{"drama", "romance", "war"},
		Version:   1,
	}

	err = app.writeJSON(w, http.StatusOK, envelope{"movie": movie}, nil)

	if err != nil {
		app.serverErrorResponse(w, r, err)

	}
}
