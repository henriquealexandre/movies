package main

import (
	"context"
	"database/sql"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/lib/pq"
)

const version = "1.0.0"

type config struct {
	port int
	env  string
}

type application struct {
	config config
	logger *log.Logger
}

func main() {
	var cfg config

	//defining config flags
	flag.IntVar(&cfg.port, "port", 4000, "API Server port")
	flag.StringVar(&cfg.env, "env", "development", "Environment (development|staging|production)")
	flag.Parse()

	//directing logs to stdout with date and time stamps
	logger := log.New(os.Stdout, "", log.Ldate|log.Ltime)

	//Reading ElephantSQL URL stored in a local var env
	dsn, err := pq.ParseURL(os.Getenv("ESQLMOVIES"))
	if err != nil {
		log.Fatal(err)
	}

	db, err := openDB(dsn)
	if err != nil {
		logger.Fatal(err)
	}

	defer db.Close()

	app := &application{
		config: cfg,
		logger: logger,
	}

	//hard coded arbitrary timeouts
	srv := &http.Server{
		Addr:         fmt.Sprintf(":%d", cfg.port),
		Handler:      app.routes(),
		IdleTimeout:  time.Minute,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 30 * time.Second,
	}

	logger.Printf("starting %s server on %s", cfg.env, srv.Addr)
	err = srv.ListenAndServe()

	//make use of our default logger
	logger.Fatal(err)

}

func openDB(dsn string) (*sql.DB, error) {
	db, err := sql.Open("postgres", dsn)
	if err != nil {
		return nil, err
	}

	//set an arbitraty deadline timeout
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	//connect will err out if it takes more than 5 seconds
	err = db.PingContext(ctx)
	if err != nil {
		return nil, err
	}

	fmt.Println("connection with DBaaS successfully established")

	return db, nil

}
