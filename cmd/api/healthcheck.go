package main

import (
	"net/http"
)

//Any dependency available to our struct 'app' is available for this handler.
func (app *application) healthCheckHandler(w http.ResponseWriter, r *http.Request) {

	data := envelope{
		"status":      "available",
		"system_info": map[string]string {
			"environment": app.config.env,
			"version":     version,
		},
	}

	h := http.Header{}
	h.Set("Content-Type", "application/json")

	err := app.writeJSON(w, http.StatusOK, data, h)
	if err != nil {
		app.serverErrorResponse(w, r, err)
	}
}
