package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"strconv"
	"strings"

	"github.com/julienschmidt/httprouter"
)

func (app *application) readIDParam(r *http.Request) (int64, error) {

	//fetching URL params
	urlparams := httprouter.ParamsFromContext(r.Context())

	//converting the string param into a valid int
	id, err := strconv.ParseInt(urlparams.ByName("id"), 10, 64)

	if err != nil || id < 1 {
		return 0, errors.New("invalid id")
	}

	return id, nil

}

type envelope map[string]interface{}

func (app *application) writeJSON(w http.ResponseWriter, status int, data envelope, headers http.Header) error {

	//it's better to use Marshall rather than encoding if we want to set the response headers manually
	//MarshalIndent can decrease performance when the endpoint gets busy. The reverse of MarshalIndent is compact()
	js, err := json.MarshalIndent(data, "", "\t")
	if err != nil {
		return err
	}
	//adding new line for the sake of terminal calls
	js = append(js, '\n')

	for key, value := range headers {
		w.Header()[key] = value
	}

	w.WriteHeader(status)
	w.Write(js)

	return nil
}

//readJSON decodes the req body and search for errors in the received data
func (app *application) readJSON(w http.ResponseWriter, r *http.Request, dst interface{}) error {
	//err := json.NewDecoder(r.Body).Decode(dst)

	//setting the body limit to 1MB
	maxBytes := 1_048_576
	r.Body = http.MaxBytesReader(w, r.Body, int64(maxBytes))

	//initialise decoder to disallow unknown JSON fields -- all fields must match the struct definition
	newdec := json.NewDecoder(r.Body)
	newdec.DisallowUnknownFields()

	//writes the settings above into the destination type
	err := newdec.Decode(dst)

	//triaging errors
	if err != nil {
		var synxtaxError *json.SyntaxError
		var unmarshalTypeError *json.UnmarshalTypeError
		var invalidUnmarshalError *json.InvalidUnmarshalError

		switch {
		case errors.As(err, &synxtaxError):
			return fmt.Errorf("body contains malformed JSON (at char %d)", synxtaxError.Offset)
			//adding this to workaround the following reported bug: https://github.com/golang/go/issues/25956
		case errors.Is(err, io.ErrUnexpectedEOF):
			return errors.New("body contains malformed JSON")
		case errors.Is(err, io.EOF):
			return errors.New("body should not be empty")
		//facilitating debug of wrong types
		case errors.As(err, &unmarshalTypeError):
			if unmarshalTypeError.Field != "" {
				return fmt.Errorf("body contains incorrect JSON type for field %q", unmarshalTypeError.Field)
			}
			return fmt.Errorf("body contains incorrect JSON type (at character %d)", unmarshalTypeError.Offset)
		//There is no specific error for unknown field, but it's in the pipeline:
		//https://github.com/golang/go/issues/29035
		case strings.HasPrefix(err.Error(), "json: unknown field "):
			fieldName := strings.TrimPrefix(err.Error(), "json: unknown field ")
			return fmt.Errorf("body contains unknown key %s", fieldName)
		//no distinct error either here:
		//https://github.com/golang/go/issues/30715
		case err.Error() == "http: request body too large":
			return fmt.Errorf("body must not be larger than %d bytes", maxBytes)
		case errors.As(err, &invalidUnmarshalError):
			//the error above is mostly related to application/logical problems.
			//Panic is more acceptable in these cirumstances: https://gobyexample.com/panic
			panic(err)
		default:
			return err
		}
	}

	// Call Decode() again, using a pointer to an empty anonymous struct as the
	// destination. If the request body only contained a single JSON value this will
	// return an io.EOF error. So if we get anything else, we know that there is
	// additional data in the request body and we return our own custom error message.
	err = newdec.Decode(&struct{}{})
	if err != io.EOF {
		return errors.New("body must only contain a single JSON value")
	}

	return nil
}
