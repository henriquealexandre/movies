package data

import "time"

type Movie struct {
	ID        int64     `json:"id"`
	CreatedAt time.Time `json:"-"`
	Title     string    `json:"title"`
	Year      int32     `json:"year,omitempty"`
	Runtime   Runtime   `json:",omitempty,string"` //the string is a directive that forces the output to be presented as string
	Genres    []string  `json:"genres,omitempty"`
	Version   rune      `json:"version"` //starts with 1 and another 1 is added each time we update the movie info
}
