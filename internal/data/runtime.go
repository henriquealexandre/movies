package data

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
)

var ErrInvalidRuntimeFormat = errors.New("invalid input format")

type Runtime rune

func (r Runtime) MarshalJSON() ([]byte, error) {
	jsonValue := fmt.Sprintf("%d mins", r)

	//wrapping the string between quotes is mandatory for this method overriding
	quotedJSON := strconv.Quote(jsonValue)

	return []byte(quotedJSON), nil
}

//overriding UnmarshalJSON method
func (r *Runtime) UnmarshalJSON(jsonValue []byte) error {
	//By default, we receive a quoted string in JSON format.
	unquotedJSONValue, err := strconv.Unquote(string(jsonValue))
	if err != nil {
		return ErrInvalidRuntimeFormat
	}

	//separating the string in parts
	stringParts := strings.Split(unquotedJSONValue, " ")

	//sanity check
	if len(stringParts) != 2 || stringParts[1] != "mins" {
		return ErrInvalidRuntimeFormat
	}

	//converting string to int
	i, err := strconv.ParseInt(stringParts[0], 10, 32)
	if err != nil {
		return ErrInvalidRuntimeFormat
	}

	//converting int64 to int32
	*r = Runtime(i)

	return nil
}
